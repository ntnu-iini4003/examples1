cmake_minimum_required(VERSION 3.5)

project(examples1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y -Wall -Wextra")

add_executable(eksempel eksempel.cpp)

add_executable(tallfil tallfil.cpp)

add_executable(toerpot toerpot.cpp)

add_executable(person person.cpp)
